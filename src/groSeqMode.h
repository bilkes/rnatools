/*
 * transcriptMode.h
 *
 *  Created on: Mar 7, 2013
 *      Author: sven
 */

#ifndef TRANSCRIPTMODE_H_
#define TRANSCRIPTMODE_H_

#include <guli/uiOperationMode.h>
#include <guli/uiParms.h>
#include <guli/gffDag.h>
#include <guli/ngsTagDensity.h>

class groSeqMode: public GULI::uiOperationMode {
	public:
		groSeqMode();
		virtual ~groSeqMode();
		virtual string describeMode();
		virtual int main(int argc, char **argv);

	protected:
		GULI::uiParms _parms;
		touchVarT  _overwrite;
//		touchVarT    _outExon;
		intvarT      _bzThreads, _bzBlocks;
		GULI::gffDag _geneModel;
		vector<samfile_t *> _sams;
		GULI::NGS::tagDensity _readCounts;
		GULI::gffNodeListT _cdna;

	private:
		void _closeSams();
		int _openSams(int argc, char **argv);

		int _getGeneModel(const char *gffFileName, const GULI::gffParser::parserMode &semantic);
		void _getReadCounts();

		GULI::constGffNodeListTiling *_cDNAcoordinates;
};


#endif /* TRANSCRIPTMODE_H_ */
