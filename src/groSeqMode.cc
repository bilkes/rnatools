/*
 * transcriptMOde.cc
 *
 *  Created on: Mar 7, 2013
 *      Author: sven
 */
#include <sys/stat.h>
#include <unordered_map>
#include <string>
#include <guli/gffParser.h>
#include <guli/gffAlgo.h>
#include <guli/dataBasic.h>
#include <guli/dataIO.h>
#include "groSeqMode.h"

using GULI::bedRegion;


groSeqMode::groSeqMode() :
	uiOperationMode("groseq"),
 	_overwrite("overwrite", "Overwrite output file if it exists",  _parms),
	_bzThreads("threads", "Number of threads to use for reading BAM files", 4,  _parms),
	_bzBlocks("blocks", "Block parameter for BAM read threads", 256,  _parms),
 	_cDNAcoordinates(0)
{}

groSeqMode::~groSeqMode(){
	if(_cDNAcoordinates) delete _cDNAcoordinates;
};

string groSeqMode::describeMode() {
	static const string r = "NOT SET.";
	return r;
}

void
groSeqMode::_closeSams() {
	for(size_t s=0; s < _sams.size(); ++s)  samclose(_sams[s]);
	return;
}

int groSeqMode::_openSams(int argc, char **argv) {
	vector<string> fileNames;
	for(int a=2; a < argc-1; ++a) {
		samfile_t *sam =  samopen(argv[a], "rb", 0);
		if(!sam) {
			std::cerr << "Can not open bamfile " << argv[a] << std::endl;
			return -1;
		}
		samthreads(sam, _bzThreads, _bzBlocks);
		_sams.push_back(sam);
		fileNames.push_back(argv[a]);
	}

	if(_readCounts.attach(_sams, fileNames) != 0) {
		std::cerr << "Problem attaching bam file to tag-density. Missing index file?\n";
		return -1;
	}
	return 0;
}

void groSeqMode::_getReadCounts() {
	GULI::gffAddNonOverlappingNodes(_geneModel, "gene", "cDNA");
	_cdna= gffFindFeatures(GULI::GFF_DOWN, _geneModel.root(), "cDNA");
	 _cDNAcoordinates = new GULI::constGffNodeListTiling (_cdna.begin(), _cdna.end());
	_readCounts.analyze(*_cDNAcoordinates);
	return;
}

int groSeqMode::_getGeneModel(const char *gffFileName, const GULI::gffParser::parserMode &semantic) {
	ifstream GFF;
	GFF.open(gffFileName);
	 if(!GFF) {
		 std::cerr << "Can not open DAG definition" << gffFileName << std::endl;
		 return -1;
	 } else {
		 GULI::gffParser::parse(GFF, _geneModel, semantic);
		 GFF.close();
		 return 0;
	 }
}

int groSeqMode::main(int argc, char **argv) {
	typedef GULI::uiParms::Enumvar<const GULI::gffParser::parserMode *> gffSyntaxT;
	static const gffSyntaxT::enumvarcfg dialectChoice[] = {
			{"ensembl", &GULI::gffParser::Ensembl },
			{"gencode", &GULI::gffParser::Gencode },
			{"native",  &GULI::gffParser::Native }
	};
	gffSyntaxT semantic("dialect", "Select GFF dialect: ensembl, gencode, native", dialectChoice, &GULI::gffParser::Gencode, _parms);

	int r = _parms.commandLine(argc, argv);
	argc -= r;
	argv += r;
	if((r < 0) or (argc < 3)) {
		std::cerr << "Usage: " << argv[0] << " [options] gffdef.gff  bamfile [bamfile ...] outFile\n" << std::endl;
		std::cerr << "Known options are" << std::endl;
		std::cerr << _parms.describeParms() << std::endl;
		return -1;
	}

	struct stat dummy;
	if(!(_overwrite ||  stat(argv[argc-1], &dummy))) {
		std::cerr << "Output file " << argv[argc-1] << " already exists. Cowardly refusing to overwrite. Use -overwrite to convince me!" << std::endl;
		return -1;
	}

	if( _openSams(argc, argv) || _getGeneModel(argv[1], *static_cast<const GULI::gffParser::parserMode *>(semantic)))  {
		_closeSams();
		return -1;
	}

	_getReadCounts();

	GULI::gffNodeListT T = gffFindFeatures(GULI::GFF_DOWN, _geneModel.root(),  "gene");
	GULI::dataBasic OUT(argc - 3, T.size());
	std::unordered_map<string, int> targets;
	for(size_t i=0; i < T.size(); ++i) {
		const GULI::gffDag::Node &nde = *T[i];
		targets[nde.id()] = i;
		const char *symbol = nde.getAttribute("Name");
		string pos = nde.chromosome() + ":" + std::to_string(nde.start()) + "-" + std::to_string(nde.end());
		OUT.rowId(i) = pos + "\t" + T[i]->id() + "\t" +  (symbol ? symbol : "N/A");
	}

	for(int s=0; s <  argc-3; ++s) {
		std::fill(OUT.beginCol(s), OUT.endCol(s), 0.);
		OUT.colId(s) = argv[s+2];
		const GULI::NGS::tagDensity::countT &C = _readCounts.density(s, GULI::NGS::tagDensity::ALL);
		const double norm = 1e6 * _readCounts.readStats(s).avReadLength() / std::accumulate(C.begin(), C.end(), 0.);
		for(size_t i=0; i < C.size(); ++i) {
			const double fac = norm * 1000. / (_cdna[i]->end() - _cdna[i]->start());
			T = gffFindFeatures(GULI::GFF_UP, _cdna[i],  "gene");
			for(size_t t = 0; t < T.size(); ++t) {
				OUT(s, targets[T[t]->id()]) += fac * C[i];
			}
		}
	}


	GULI::IO::write(argv[argc-1], OUT);

	int err = 0;
//error:
	_closeSams();
	return err;

}

static groSeqMode thatsme;
