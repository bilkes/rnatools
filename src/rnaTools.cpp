//============================================================================
// Name        : rnaTools.cpp
// Author      : Sven Bilke
// Version     :
// Copyright   : GPL V3.0
// Description : Hello World in C++, Ansi-style
//============================================================================
using namespace std;
#include <guli/uiOperationMode.h>

/** Welcome, Stranger. If you wonder where to start reading (or, how does main() know about
 *  all the nice (?) things it can do), have a look at peakfinderMode.*.
 *
 *  The user interface (-option ....) is built dynamically when  objects defined in uiparms.*,
 *  namely  uiparms::Variable<>, are instantiated. Places to look are in the peakfinderMode derived
 *  classes (such as templateModule., which sets up the "standard" module).
 *
 *  If you are interested in adding an analysis mode, chances are you want to derive a class from
 *  templateModule and re-implement the \a process method, which takes over after the hit-statistics
 *  of the runs have been read into memory.
 */


int main(int argc, char **argv) {
	return GULI::uiOperationMode::entryPoint(argc, argv);
}

static GULI::uiHelpMode  _help;
